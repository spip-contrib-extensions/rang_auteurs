<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// D
	'deplacer_element' => 'Déplacer cet élément',

	// L
	'lien_tout_desordonner' => 'Réinitialiser l’ordre',
	'lien_ordonner' => 'Ordonner les auteurs',

	// R
	'rang_auteurs_titre' => 'Rang sur les auteurs',

);
