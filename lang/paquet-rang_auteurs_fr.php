<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'rang_auteurs_description' => '',
	'rang_auteurs_nom' => 'Rang sur les auteurs',
	'rang_auteurs_slogan' => 'De la préséance que diable !',
);
