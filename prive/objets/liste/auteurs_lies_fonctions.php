<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2019                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('prive/objets/liste/auteurs_fonctions');

/**
 * Détecter si la liste des auteurs a un rang ou non
 * @param string $objet_lien
 * @return bool
 */
function rang_auteurs_has_rang($objet_lien) {
	$table_liens = table_objet_sql($objet_lien) . '_liens';
	$trouver_table = charger_fonction('trouver_table', 'base');
	$desc = $trouver_table($table_liens);
	return isset($desc['field']['rang_lien']);
}

/**
 * trouver les ids avec leurs rangs
 * @param $ids
 * @param $objet_source
 * @param $objet
 * @param $id_objet
 * @param $objet_liens
 * @return ?array
 */
function rang_auteurs_lister_objets_lies($objet_source, $objet, $id_objet, $objet_lien) {
	if (rang_auteurs_has_rang($objet_lien)) {
		$res = lister_objets_liens($objet_source, $objet, $id_objet, $objet_lien);
		$colonne_id = ($objet_source == $objet_lien ? id_table_objet($objet_source) : 'id_objet');
		$ids_with_rang = array_column($res, 'rang_lien', $colonne_id);
		asort($ids_with_rang);
		return $ids_with_rang;
	}
	return null;
}
